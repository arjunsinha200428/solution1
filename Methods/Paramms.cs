﻿using System;

namespace Methods
{
    public class Sum1{
        public static void Sum(params int[]  numbers)
        {
        int result = 0;
        foreach (var n in numbers)
        {
            result += n;
        }
        Console.WriteLine(result);
        }
 
    
}
}