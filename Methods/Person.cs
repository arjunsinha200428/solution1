﻿namespace Methods 

{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int[] nums = { 1, 2, 3, 4, 5};
            Sum1.Sum(nums);
            Sum1.Sum(1, 2, 3, 4);
        }
    }

}