﻿using System;
using System.Runtime.InteropServices;

namespace eventsDelegatesLamda
{
    public class Delegates
    {
        delegate int MyTask(int a, int b);
        delegate int Message(int a, int b);
        

        private int Test(int a, int b)
        {
            Message mes;
            mes = Plus;
            return mes(a,a);

        }
        private int Plus(int a, int b)
        {
            return a + b;
        }

        private int Minus(int a, int b)
        {
            return a - b;
        }

        private void Run(MyTask task, int s)
        {
            Console.WriteLine("Start");
            Console.WriteLine(task(1, s));
            Console.WriteLine("end");
        }

       
        public void AllTasks()
        {
            Run(Plus, 3);
            Run(Minus, 5);
        }
    }
}