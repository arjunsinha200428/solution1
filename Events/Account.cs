﻿namespace Events;

public class Account
{
    public delegate void AccountHandler(string message);
    
    public event AccountHandler? Notify;
    public int Sum { get; private set; }
    public Account(int sum) => Sum = sum;

    public void Put(int sum)
    {
        Sum += sum;
        Notify($"Recieved: {sum}");
        Console.WriteLine();
    }

    public void Take(int sum)
    {
        if (sum <= Sum)
        {
            Sum -= sum;
            Notify($"Spent: {sum}");
        }
        
    }

 
    public  void SukaNahui(string ass)
    {
        Console.WriteLine(ass);
    }

}