﻿class Program
{
    public static void Main(string[] args)
    {
        var outer = Task.Factory.StartNew(() =>
        {
            Console.WriteLine("Test1");
            var inside = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("Test2");
                Thread.Sleep(1000);
                Console.WriteLine("Test3");
            }, TaskCreationOptions.AttachedToParent);
        });
        outer.Wait();
        Console.WriteLine("Test ended");
    }
}