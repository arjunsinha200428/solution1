﻿namespace AcPeFunc;

public class PrediCate
{
    delegate bool Predicate<in T>(T obj);

    public void MyPerdicate()
    {
        Predicate<int> isPositive = (int x) => x > 0;
        Console.WriteLine(isPositive(20));
        Console.WriteLine(isPositive(-20));
    }
    
}