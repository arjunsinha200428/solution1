﻿namespace AcPeFunc;

public class Popka
{
    public void MyAction(int x, int y)
    {
        DoOperation(x,y, Add);
    }
    public delegate void Action();
    public delegate void Action<in T>(T obj);
    public void Add(int x, int y) => Console.WriteLine($"{x} + {y} = {x + y}");
    public void Multiply(int x, int y) => Console.WriteLine($"{x} * {y} = {x * y}");
    public void DoOperation(int x, int y, Action<int, int> op) => op(x, y);

}