﻿using System.IO;

class Program
{
    public static void Main(string[] args)
    {
        string dirName = "C:\\";
        if (Directory.Exists(dirName))
        {
            Console.WriteLine("Catalog");
            string[] dirs = Directory.GetDirectories(dirName);
            foreach (var dir in dirs)
            {
                Console.WriteLine(dir);
            }
            Console.WriteLine();
            Console.WriteLine("Files: ");
            string[] files = Directory.GetFiles(dirName);
            foreach (var f in files)
            {
                Console.WriteLine(f);
            }
        }
    }
}