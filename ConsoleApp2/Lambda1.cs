﻿using System.Numerics;
using System.Threading.Channels;

namespace ConsoleApp2;

using System;

    
    public class Lambda1
    {
        delegate T Message<T>(T a, T b);

        delegate void LambdaDelegate(int x, int y);
        T someMethod<T>(T a, T b) where T : INumber<T>
        {
            return a + b;
        }
        private void task1<T>(Message<T> task, T a, T b)
        {
            Console.WriteLine(task(a, b));
            

        }

        public void AllTask()
        {
            
            task1(someMethod, 12, 123);
        }

        public void Lambda()
        {
            LambdaDelegate hello = (int a , int b) =>
            {
                Console.WriteLine(a + b);
            };
        }
    }
