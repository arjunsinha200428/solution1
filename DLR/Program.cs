﻿namespace DLR
{
    class Program
    {
        public static void Main(string[] args)
        {
            dynamic obj = 3;
            Console.WriteLine(obj);

            obj = "Hello World";
            Console.WriteLine(obj);
            obj = new Person("Tom", 37);
            Person tom = new Person("TT", 23);
            Console.WriteLine(tom.GetSalary(3434, "string"));
        }
    }
    class Person
    {
        public string Name { get;}
        public dynamic Age { get; set; }
        public Person(string name, dynamic age)
        {
            Name = name; Age = age;
        }
 
        // выводим зарплату в зависимости от переданного формата
        public dynamic GetSalary(dynamic value, string format)
        {
            if (format == "string") return $"{value} euro";
            else if (format == "int") return value;
            else return 0.0;
        }
 
        public override string ToString() => $"Name: {Name}  Age: {Age}";
    }
}