﻿using System;

namespace exeptionHandling
{
    public class square
    {
        public void Square(string data)
        {
            if (int.TryParse(data, out var x))
            {
                Console.WriteLine($"{x*x}");
            }
            else
            {
                Console.WriteLine("error. Only numbers");
            }
        }
    }
}