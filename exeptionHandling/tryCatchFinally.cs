﻿using System;

namespace exeptionHandling
{
    public class tryCatchFinally
    {
        public void TryCatchFinally()
        {
            int x = 6 ;
            int y;
            try
            {
                y = x / 0;
            }
            catch (Exception ex)
            {
                y = 0;
                Console.WriteLine($"Exception: {ex.Message}.. Method: {ex.TargetSite}.. {ex.InnerException}");
            }
            
            

            Console.WriteLine(y);
        }
        
    }
}