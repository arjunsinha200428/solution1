﻿using System;

namespace exeptionHandling
{
    public class Person18
    {
        private int age;
        public string Name { get; set; } = "";

        public int Age
        {
            get => age;
            set
            {
                if (value < 18)
                    throw new Exception("Registration under 18 not allowed");
                else
                    age = value;
            }
        }
    }
}