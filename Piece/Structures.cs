﻿using System;

namespace Ass
{

        struct structPerson
        {
            public string name;
            public int age;

            public structPerson(string name = "tom", int age = 12)
            {
                this.name = name;
                this.age = age;
            }

            public void Print()
            {
                Console.WriteLine($"Name: {name}, Age: {age}");
            }
        }
    
}