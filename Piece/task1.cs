﻿using ClassLibrary1;
using System;
namespace Ass
{
    public class task1
    {
        public static void TaskOne()
        {

            Person tom = new Person { name = "Ivan", age = 28, dungeon = { title = "Gym" } };
            tom.Print();
            string name, dungeon;
            int age;
            tom.Deconstructor(out name, out age, out dungeon);
            Console.WriteLine($"Name: {name}, Age: {age} Dungeon: {dungeon}");
        }
    }
}