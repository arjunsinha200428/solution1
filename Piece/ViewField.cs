﻿using System;

namespace Ass
{
    public class ViewFieldPerson
    {
        public string type = "Person";

        public void PrintName()
        {
            string name = "Tom";
            {
                string shortName = "Tomas";
                Console.WriteLine(type);
                Console.WriteLine(name);
                Console.WriteLine(shortName);
                
            }
            Console.WriteLine(type);
            Console.WriteLine(name);
            //Console.WriteLine(shortName); //так нельзя, переменная c определена в блоке кода
            //Console.WriteLine(surname);     //так нельзя, переменная surname определена в другом методе
            
        }

        public void PrintSurname()
        {
            string surname = "Smith";
            Console.WriteLine(type);
            Console.WriteLine(surname);
        }
    }
}