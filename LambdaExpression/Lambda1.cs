﻿using System;

namespace LambdaExpression
{
    public class someType<T>
    {
        public T Id { get; set; }

        public someType(T id)
        {
            Id = id;
        }
    }
    public class Lambda1
    {
        private someType<string> tom = new someType<string>("sds");
        delegate T Message<T>(someType<T> a, someType<T> b);

        T Nuttes<T>(T a, T b) where T : INumber<T>
        {
            return a + b;
        }


        public void Lambda()
        {
           
 
        }

    }
}