﻿using OOP;


using System;
namespace OOP
{
    public class Persons
    {
        public static void Personisch()
        {
            Employee tom = new Employee("Tom", 28, "Gym");
            Person person = new Client("Sparkasse", "Ivan");

            tom.Print();
            Client clientFirst = new Client("Achtung", "Sparkasse");
            clientFirst.Print();
        }
    }
}