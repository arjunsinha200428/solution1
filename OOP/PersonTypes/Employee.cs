﻿using System;

namespace OOP
{
    class Employee : Person
    {
        private string company;
        private string name;

        public Employee(string name, int age, string company)
            : base(name, age)
        {
            this.company = company;
            this.name = name;
        }

        public override void Print()
        {
            Console.WriteLine($"Company: {company}, Name: {name}");
        }
    }
}