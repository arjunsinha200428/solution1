﻿using System;

namespace OOP
{
     public class Client : Person
    {
        public string Bank { get; set; }
        public string Name { get; set; }

        public Client(string name, string bank) : base(name)
        {
            Bank = bank;
            Name = name;

        }

        public override void Print()
        {
            Console.WriteLine($"Clients name is {Name}, bank name is {Bank}");
        }
    }
}