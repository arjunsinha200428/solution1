﻿using System;

namespace OOP
{
    public class Person
    {
        private string name = "S";
        private int age = 18;
        public Person(string name)
        {
            this.name= name;
        }

        public Person(string name, int age) : this(name)
        {
            this.age = age;
        }
        public virtual void Print()
        {
            Console.WriteLine($"Age: {age}, Name: {name}");
        }
    }
}