﻿using System;
using System.Runtime.InteropServices;

namespace OOP
{

    public class List<T>
    {
        private T[] items;
        private int count;

        public List(int elements)
        {
            items = new T[elements];
            count = 0;
        }

        public void Add(T item)
        {
            items[count++] = item;
        }

        public T Get(int index)
        {
            if (index > -0 && index < count)
            {
                return items[index];
            }
            else throw new IndexOutOfRangeException();
        }

        public void Show()
        {
            for (int i = 0; i < count; i++)
            {
                Console.WriteLine(items[i]);
            }
        }
    }

}

    