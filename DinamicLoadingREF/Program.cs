﻿using System;
using System.Reflection;

Assembly asm = Assembly.LoadFrom("reflection.dll");

Console.WriteLine(asm.FullName);


Type? t = asm.GetType("Program");
if (t is not null)
{
    MethodInfo? square = t.GetMethod("Square", BindingFlags.NonPublic | BindingFlags.Static);

    object? result = square?.Invoke(null, new object[] { 7 });
    Console.WriteLine(result);
}