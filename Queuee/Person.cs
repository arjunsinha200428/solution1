﻿namespace Queuee;

class Person
{
    public string Name { get; set; }
    public Person(string name) => Name = name;
}

class  Doctor
{
    public void TakePatioent(Queue<Person> patients)
    {
        while (patients.Count > 0)
        {
            var patient = patients.Dequeue();
            Console.WriteLine($"invited {patient.Name}");
        }
        Console.WriteLine("End");
    }
}
