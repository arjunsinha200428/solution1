﻿using System;

namespace ConsoleApplication1
{
    public class person
    {
        public string name;
        public int age;

        public person()
        {
            Console.WriteLine("Created Object person");
            name = "Tom";
            age = 37;
      
        }

        public void Print()
        {
            Console.WriteLine($"Name: {name},Age: {age}");
        }
    }
}
