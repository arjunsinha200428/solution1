﻿using System;

namespace ClassLibrary1
{
    public class Person
    {
        
        public string name;
        public int age;
        public Dungeon dungeon;
        public Person(string name = "Uknown", int age = 18)     // третий конструктор
        {
            this.name = name;
            this.age = age;
            dungeon = new Dungeon();

        }
        public void Print() => Console.WriteLine($"Имя: {name}  Возраст: {age}, " +
                                                 $"Dungeon: {dungeon.title}");

        public void Deconstructor(out string personName, out int personAge, out string personDungeon)
        {
            personAge = age;
            personName = name;
            personDungeon = dungeon.title;
        }
    }

    public class Dungeon
    {
        public string title = "Dungeon";
    }
}
    
