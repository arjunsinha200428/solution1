﻿using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
using Microsoft.Scripting.Runtime;

int y = 22;
ScriptEngine engine = Python.CreateEngine();
ScriptScope scope = engine.CreateScope();
engine.ExecuteFile("hellotwo.py", scope);
dynamic square = scope.GetVariable("square");
dynamic result = square(y);
Console.WriteLine(result);
