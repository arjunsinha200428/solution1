﻿namespace Indexators;

public class Humans
{
    
}

class Player
{
    public string Name { get; set; } // имя
    public int Number { get; set; } // номер

    public Player(string name, int number)
    {
        Name = name;
        Number = number;
    }
}
class Team
{
    Player[] players = new Player[11];
 
    public Player this[int index]
    {
        get
        {
            if (index >= 0 && index < players.Length)
                return players[index];
            else 
                throw new ArgumentOutOfRangeException();
           
        }
        set
        {
            if (index >= 0 && index < players.Length)
                players[index] = value;
            
        }
    }
}