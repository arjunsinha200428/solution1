﻿using System;

namespace MyInterface
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Some");
            var tom = new Person("A", 23);
            var bob = new Person("Bob", 32);
            var sam = new Person("Sam", 19);
            Person[] people = { tom, bob, sam };
            Array.Sort(people);
            foreach (Person person in people)
            {
                Console.WriteLine($"{person.Name}, {person.Age}");
            }


        }
    }
   
}