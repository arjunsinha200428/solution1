﻿namespace MyInterface;

public interface IMovable
{
   void Move();
}

/*class Person : IMovable
{
   public void Move()
   {
      Console.WriteLine("Man Walking");
   }
}
*/
struct Car : IMovable
{
   public void Move() => Console.WriteLine("Car is Driving");
}
