﻿namespace MyInterface;

public interface IInterfaces
{
    
}

class Person : IComparable<Person>
{
    public string Name { get; set; }
    public int Age { get; set; }

    public Person(string name, int age)
    {
        Name = name;
        Age = age;
    }

    public int CompareTo(Person? person)
    {
        if(person is null) throw new ArgumentException("Некорректное значение параметра");
        return Age.CompareTo(person.Age);
    }
    public object Clone()
    {
        return new Person(Name, Age);
    }
}

interface IClonable
{
    object Clone();
}

public interface IComparable
{
    int CompareTo(Object? o);
}
