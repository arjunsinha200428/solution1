﻿namespace Operators;

public class State
{
    public decimal Population { get; set; }
    public decimal Area { get; set; }


    public static State operator +(State s1, State s2)
    {
        return new State
        {
            Area = s1.Area + s2.Area,
            Population = s1.Population + s2.Population
        };
    }

    public static bool operator >(State s1, State s2)
    {
        return s1 > s2;
    }

    public static bool operator <(State s1, State s2)
    {
        return s1 < s2;
    }
    
}
class Bread
{
    public int Weight { get; set; } // масса
    public static Sandwich operator +(Bread s1, Butter s2)
    {
        return new Sandwich
        {
            Weight = s1.Weight + s2.Weight
        };
    }
}
class Butter
{
    public int Weight { get; set; } // масса
}
class Sandwich
{
    public int Weight { get; set; } // масса

    
}