﻿// See https://aka.ms/new-console-template for more information

namespace Operators
{
    class Program
    {
        public static void Main(string[] args)
        {
        
            State state1 = new State{Area = 200, Population = 3000};
            State state2 = new State{Area = 343, Population = 3434};
            State state3 = state1 + state2;
            bool a = state1.Population < state3.Population;
            Console.WriteLine(a);
            Console.WriteLine(state3.Population);
            Bread bread = new Bread { Weight = 80 };
            Butter butter = new Butter { Weight = 20 };
            Sandwich sandwich1 = bread + butter;
            Console.WriteLine(sandwich1.Weight);  // 100
        }
    } 
}
