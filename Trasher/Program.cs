﻿Test();
GC.Collect();
Console.Read();

void Test()
{
    Person Tom = new Person("Tom");
    Console.WriteLine(Tom.Name);
}

public class Person
{
    public string Name { get; }
    public Person(string name) => Name = name;

    ~Person()
    {
        Console.WriteLine($"{Name} has been deleted");
    }
}