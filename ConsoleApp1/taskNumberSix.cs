﻿using ConsoleApp1.supMethods;

namespace ConsoleApp1;

public class taskNumberSix
{
    public static void TaskNumberSix()
    {
        Console.WriteLine("1(+) 2(-) 3(*)");
        int operation = int.Parse(Console.ReadLine());
        var (a, b) = input6.inputSix();
        switch (operation)
        {
            case 1:
                
                Console.Write(a + b);
                break;
            case 2:
                Console.Write(a - b);
                break;
            case 3:
                Console.Write(a * b);
                break;
            default: 
                Console.WriteLine("Error"); 
                break;
        }
    }
}