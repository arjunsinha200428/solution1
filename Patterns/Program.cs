﻿using Patterns;

class Program
{
    public static void Main(string[] args)
    {
        Company microsoft = new Company("Microsoft");
        Employee tom = new Employee("Tom", microsoft);
        SayHello(tom);
        void SayHello(Employee employee)
        {
            if (employee is Employee {Company:{Title:"Microsoft"}})
            {
                Console.WriteLine($"{employee.Name} Works in Microsoft");
            }
            
        }

    }

    
}