﻿namespace Patterns;

public class Employee
{
    public string Name { get; }
    public Company Company { get; set; }

    public Employee(string name, Company company)
    {
        Company = company;
        Name = name;
    }

}

public class Company
{
    public string Title { get; }
    public Company(string title) => Title = title;

    
}