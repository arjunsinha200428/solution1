﻿class Program
{
    public static void Main(string[] args)
    {
        var people = new List<Person>
        {
            new Person ("Tom", 23, new List<string> {"english", "german"}),
            new Person ("Bob", 27, new List<string> {"english", "french" }),
            new Person ("Sam", 29, new List<string>  { "english", "spanish" }),
            new Person ("Alice", 24, new List<string> {"spanish", "german" })
        };
        var selectedPeople = from p in people
            from lang in p.Languages
            where p.Age > 25
            where lang == "spanish"
            select p;

        foreach (var person in selectedPeople)
        {
            Console.WriteLine($"{person.Name} in age of {person.Age}");
        }
    }
}

record class Person(string Name, int Age, List<string> Languages);